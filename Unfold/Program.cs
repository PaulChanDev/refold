﻿using Common;
using System;
using System.IO;

namespace Unfold
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string d in Directory.GetDirectories(Vars.rootDir))
            {
                foreach (string fileFullPath in Directory.GetFiles(d))
                {
                    string fileNmae = Path.GetFileName(fileFullPath);

                    File.Move(fileFullPath, Vars.rootDir + fileNmae);

                    Console.WriteLine("Move " + fileNmae + " to " + Vars.rootDir); 
                }
            }

            Console.ReadLine();
        }
    }
}
