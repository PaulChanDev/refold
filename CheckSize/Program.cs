﻿using Common;
using System;
using System.IO;

namespace CheckSize
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string d in Directory.GetDirectories(Vars.rootDir))
            {
                foreach (string fileFullPath in Directory.GetFiles(d))
                {
                    string fileNmae = Path.GetFileName(fileFullPath);

                    double length = new FileInfo(fileFullPath).Length;
                    length = length / 1024.0 / 1024.0;

                    Console.WriteLine(fileNmae + " - " + length.ToString("F2") + "MB");
                }
            }

            foreach (string fileFullPath in Directory.GetFiles(Vars.rootDir))
            {
                string fileNmae = Path.GetFileName(fileFullPath);

                double length = new FileInfo(fileFullPath).Length;
                length = length / 1024.0 / 1024.0;

                Console.WriteLine(fileNmae + " - " + length.ToString("F2") + "MB");
            }
        }
    }
}
