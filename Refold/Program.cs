﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Refold
{
    class Program
    {
        static string rootDir = Vars.rootDir;
        static int groupSize = Vars.groupSize;
        static List<string> listPrefix = new List<string>();
        static List<string> listFileAdd = new List<string>();
        

        static void Main(string[] args)
        {
            string[] fileEntries = Directory.GetFiles(rootDir);

            foreach (string fileFullPath in fileEntries)
            {
                string fileNmae = Path.GetFileName(fileFullPath);

                int index = CheckNumPosition(fileNmae);

                if (index > 0)
                {
                    string fileNamePrefix = fileNmae.Substring(0, index);
                    fileNamePrefix = RemoveSymbol(fileNamePrefix);

                    listPrefix.Add(fileNamePrefix);

                    listFileAdd.Add(fileNmae);
                }
            }

            foreach (var prefix in listPrefix.GroupBy(n => n)
                .Where(n => n.Count() >= groupSize)
                .Select(n => new { name = n.Key, count = n.Count() }))
            {
                CreateDir(prefix.name);
            }

            foreach(string dir in Directory.GetDirectories(rootDir))
            {
                string dirName = Path.GetFileName(dir);

                foreach (string fileName in listFileAdd)
                {
                    if (fileName.StartsWith(dirName))
                    {
                        string sourceFile = rootDir + fileName;
                        string destFile = rootDir + dirName + "\\" + fileName;

                        File.Move(sourceFile, destFile);

                        Console.WriteLine("Move" + fileName + " to " + dirName);
                    }
                }
            }

            Console.ReadLine();
        }

        public static void CreateDir(string dirName)
        {
            string fullPath = rootDir + dirName;

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
        }

        public static int CheckNumPosition(string text)
        {
            Regex re = new Regex(@"\d+");
            Match m = re.Match(text);
            if (m.Success)
            {
                return m.Index;
            }

            return -1;
        }

        public static string RemoveSymbol(string text)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            text = rgx.Replace(text, "");

            return text;
        }
    }
}
